/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Presentacion;

import static Presentacion.Principal.user;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author marti
 */
public class VerGastos extends javax.swing.JFrame {
    public String dueño,avatar;
    public double total;
    /**
     * Creates new form VerGastos
     */
    public VerGastos(String dueño, String avatar) {
        initComponents();
        this.setLocationRelativeTo(null);
        this.dueño=dueño;
        this.avatar=avatar;
        
     
         cargarGastos();
         lblTotal.setText(""+total);
    }
    
    public void cargarGastos(){
       
       File archivo = new File("src\\Archivos\\gastos.txt");
       try{
       // cmbMascotas.addItem(" ");
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
                   DefaultTableModel table = (DefaultTableModel) tblGastos.getModel();
                   table.setRowCount(0);//para limpiar la tabla
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               String[] RenglonTabla= new String[100];
               //valida si ingresaste un usuario que ya existe
               //System.out.println(user+" = "+data[2]);
               if (user.equals(data[3])){
                 // cmbMascotas.addItem(data[1]);
                          RenglonTabla[0]=data[2];//nombre Mascota
                          RenglonTabla[1]=data[1];//descripcion
                          RenglonTabla[2] =data[4];//fecha
                          RenglonTabla[3] =data[5];//importe
                         total+=Double.parseDouble(data[5]);
                          table.addRow(RenglonTabla);
                  
                   
               }
               
           }
        
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          }
    }


    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlHeader = new javax.swing.JPanel();
        imgLogo = new javax.swing.JLabel();
        lblTitulo1 = new javax.swing.JLabel();
        lblTitulo2 = new javax.swing.JLabel();
        btnCerrar = new javax.swing.JLabel();
        pnlBody = new javax.swing.JPanel();
        lblTotal = new javax.swing.JLabel();
        btnRegresar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblGastos = new javax.swing.JTable();
        lblUsuario2 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        pnlFoot = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlHeader.setBackground(new java.awt.Color(51, 153, 255));
        pnlHeader.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlHeader.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        imgLogo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LogoT.png"))); // NOI18N
        pnlHeader.add(imgLogo, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 30, -1, 70));

        lblTitulo1.setFont(new java.awt.Font("Segoe UI Black", 1, 48)); // NOI18N
        lblTitulo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/market.png"))); // NOI18N
        lblTitulo1.setText("Ver gastos totales");
        pnlHeader.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 550, 70));

        lblTitulo2.setFont(new java.awt.Font("Segoe UI Black", 1, 36)); // NOI18N
        lblTitulo2.setText("Pet App");
        pnlHeader.add(lblTitulo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, 170, 40));

        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close.png"))); // NOI18N
        btnCerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCerrarMouseClicked(evt);
            }
        });
        pnlHeader.add(btnCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 0, 20, 20));

        getContentPane().add(pnlHeader, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));

        pnlBody.setBackground(new java.awt.Color(153, 204, 255));
        pnlBody.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlBody.setPreferredSize(new java.awt.Dimension(600, 300));
        pnlBody.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTotal.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblTotal.setText("Total");
        pnlBody.add(lblTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 240, -1, 40));

        btnRegresar.setBackground(new java.awt.Color(153, 204, 255));
        btnRegresar.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        btnRegresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/return.png"))); // NOI18N
        btnRegresar.setText("Regresar");
        btnRegresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRegresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegresarActionPerformed(evt);
            }
        });
        pnlBody.add(btnRegresar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, -1, -1));

        tblGastos.setAutoCreateRowSorter(true);
        tblGastos.setBackground(new java.awt.Color(102, 153, 255));
        tblGastos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mascota", "Descripción Gasto", "Fecha", "Importe"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tblGastos);

        pnlBody.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 580, 210));

        lblUsuario2.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblUsuario2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/coin.png"))); // NOI18N
        lblUsuario2.setText("Total:  $");
        pnlBody.add(lblUsuario2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, -1, 40));
        pnlBody.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 230, -1, 10));
        pnlBody.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 240, 170, -1));

        getContentPane().add(pnlBody, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, -1, -1));

        pnlFoot.setBackground(new java.awt.Color(51, 153, 255));
        pnlFoot.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlFoot.setPreferredSize(new java.awt.Dimension(600, 50));
        pnlFoot.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Derechos Reservados, Martin Alejandro Larguero Ponce");
        pnlFoot.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, -1));

        getContentPane().add(pnlFoot, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 400, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCerrarMouseClicked
        Principal ventana = new Principal(dueño, avatar);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_btnCerrarMouseClicked

    private void btnRegresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegresarActionPerformed
        Principal ventana = new Principal(dueño, avatar);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        this.dispose();
    }//GEN-LAST:event_btnRegresarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VerGastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VerGastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VerGastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VerGastos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VerGastos(null,null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnCerrar;
    private javax.swing.JButton btnRegresar;
    private javax.swing.JLabel imgLogo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblTitulo2;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JLabel lblUsuario2;
    private javax.swing.JPanel pnlBody;
    private javax.swing.JPanel pnlFoot;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JTable tblGastos;
    // End of variables declaration//GEN-END:variables
}
