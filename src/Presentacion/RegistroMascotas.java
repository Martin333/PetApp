/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Presentacion;

import LogicaNegocio.AbPetApp;
import LogicaNegocio.Mascota;
import javax.swing.JOptionPane;

/**
 *
 * @author marti
 */
public class RegistroMascotas extends javax.swing.JFrame {
    public String dueño,ava;
    /**
     * Creates new form RegistroMascotas
     */
    public RegistroMascotas(String user,String avatar) {
        initComponents();
        this.setLocationRelativeTo(null);
        dueño =user;
        ava= avatar;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnlHeader = new javax.swing.JPanel();
        lblCerrar = new javax.swing.JLabel();
        lblTitulo1 = new javax.swing.JLabel();
        imgLogo1 = new javax.swing.JLabel();
        lblTitulo3 = new javax.swing.JLabel();
        pnlBody = new javax.swing.JPanel();
        lblUsuario = new javax.swing.JLabel();
        txtRaza = new javax.swing.JTextField();
        btnCancelar = new javax.swing.JButton();
        lblPassword1 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        lblPassword3 = new javax.swing.JLabel();
        txtNombre = new javax.swing.JTextField();
        lblPassword2 = new javax.swing.JLabel();
        lblPassword4 = new javax.swing.JLabel();
        lblUsuario1 = new javax.swing.JLabel();
        txtEspecie = new javax.swing.JTextField();
        btnAgregar = new javax.swing.JButton();
        spnEdad = new javax.swing.JSpinner();
        cmbGenero = new javax.swing.JComboBox();
        lblTitulo2 = new javax.swing.JLabel();
        pnlFoot = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);

        pnlHeader.setBackground(new java.awt.Color(51, 153, 255));
        pnlHeader.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlHeader.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/close.png"))); // NOI18N
        lblCerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCerrarMouseClicked(evt);
            }
        });
        pnlHeader.add(lblCerrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 0, 20, 20));

        lblTitulo1.setFont(new java.awt.Font("Segoe UI Black", 1, 48)); // NOI18N
        lblTitulo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/chick.png"))); // NOI18N
        lblTitulo1.setText("Registro Mascota");
        pnlHeader.add(lblTitulo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 30, 530, 70));

        imgLogo1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/LogoT.png"))); // NOI18N
        pnlHeader.add(imgLogo1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 30, -1, 70));

        lblTitulo3.setFont(new java.awt.Font("Segoe UI Black", 1, 36)); // NOI18N
        lblTitulo3.setText("Pet App");
        pnlHeader.add(lblTitulo3, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 0, 170, 40));

        pnlBody.setBackground(new java.awt.Color(153, 204, 255));
        pnlBody.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlBody.setPreferredSize(new java.awt.Dimension(600, 300));
        pnlBody.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblUsuario.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/dog.png"))); // NOI18N
        lblUsuario.setText("Nombre");
        pnlBody.add(lblUsuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 30, -1, 30));

        txtRaza.setBackground(new java.awt.Color(204, 255, 255));
        txtRaza.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        pnlBody.add(txtRaza, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 150, 210, 30));

        btnCancelar.setBackground(new java.awt.Color(255, 153, 153));
        btnCancelar.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/cancel (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        pnlBody.add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 390, -1, 50));

        lblPassword1.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblPassword1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/instructions.png"))); // NOI18N
        lblPassword1.setText("Descripción");
        pnlBody.add(lblPassword1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, -1, 30));

        txtDescripcion.setBackground(new java.awt.Color(204, 255, 255));
        txtDescripcion.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        pnlBody.add(txtDescripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 270, 210, 30));

        lblPassword3.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblPassword3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/pet-shop.png"))); // NOI18N
        lblPassword3.setText("Raza");
        pnlBody.add(lblPassword3, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 150, -1, 30));

        txtNombre.setBackground(new java.awt.Color(204, 255, 255));
        txtNombre.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        pnlBody.add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, 210, 30));

        lblPassword2.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblPassword2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/gift.png"))); // NOI18N
        lblPassword2.setText("Edad");
        pnlBody.add(lblPassword2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 210, -1, 30));

        lblPassword4.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblPassword4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/sex.png"))); // NOI18N
        lblPassword4.setText("Género");
        pnlBody.add(lblPassword4, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 330, -1, -1));

        lblUsuario1.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblUsuario1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/fish-bowl.png"))); // NOI18N
        lblUsuario1.setText("Especie");
        pnlBody.add(lblUsuario1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 90, -1, 30));

        txtEspecie.setBackground(new java.awt.Color(204, 255, 255));
        txtEspecie.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        pnlBody.add(txtEspecie, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 90, 210, 30));

        btnAgregar.setBackground(new java.awt.Color(153, 204, 255));
        btnAgregar.setFont(new java.awt.Font("Segoe UI Black", 1, 14)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/plus.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });
        pnlBody.add(btnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 390, -1, -1));

        spnEdad.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        spnEdad.setModel(new javax.swing.SpinnerNumberModel(1, 1, 200, 1));
        pnlBody.add(spnEdad, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 210, 80, 30));

        cmbGenero.setBackground(new java.awt.Color(204, 255, 255));
        cmbGenero.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
        cmbGenero.setForeground(new java.awt.Color(153, 153, 153));
        cmbGenero.setModel(new javax.swing.DefaultComboBoxModel(new String[] { " ", "Masculino", "Femenino" }));
        cmbGenero.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pnlBody.add(cmbGenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 330, 210, 40));

        lblTitulo2.setFont(new java.awt.Font("Segoe UI Black", 1, 24)); // NOI18N
        lblTitulo2.setText("Años");
        pnlBody.add(lblTitulo2, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 200, 70, 40));

        pnlFoot.setBackground(new java.awt.Color(51, 153, 255));
        pnlFoot.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 204)));
        pnlFoot.setPreferredSize(new java.awt.Dimension(600, 50));
        pnlFoot.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setText("Derechos Reservados, Martin Alejandro Larguero Ponce");
        pnlFoot.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 20, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(pnlFoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(pnlBody, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 550, Short.MAX_VALUE)
                .addComponent(pnlFoot, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(pnlHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(pnlBody, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 50, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void lblCerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCerrarMouseClicked
        Limpiar();
        Principal ventana = new Principal(dueño, ava);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        this.dispose();

    }//GEN-LAST:event_lblCerrarMouseClicked

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
         Limpiar();
        Principal ventana = new Principal(dueño, ava);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        this.dispose();

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
         if(txtEspecie.getText()!=""&&txtNombre.getText()!=""&&txtRaza.getText()!=""&&txtDescripcion.getText()!=""&&(Integer)spnEdad.getValue()!=0& cmbGenero.getSelectedItem().toString()!=" "){
       
        AbPetApp objUsuario = new Mascota(txtNombre.getText(),dueño,txtEspecie.getText(),txtRaza.getText(),spnEdad.getValue().toString(), cmbGenero.getSelectedItem().toString(),txtDescripcion.getText(),"activo");
        String respuesta= objUsuario.registrarMascota();
        if (respuesta =="exito"){
        Limpiar();
        Principal ventana = new Principal(dueño, ava);
        ventana.setVisible(true);
        ventana.setDefaultCloseOperation(ventana.HIDE_ON_CLOSE);
        this.dispose();
        }
        else if(respuesta =="errorUsuario"){
            txtNombre.setText("");
            txtNombre.setFocusable(true);
            
        }
        else{
            Limpiar();
        }
       }
        else{
            JOptionPane.showMessageDialog(null,"Todos los campos se deben llenar!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
        }

    }//GEN-LAST:event_btnAgregarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(RegistroMascotas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(RegistroMascotas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(RegistroMascotas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(RegistroMascotas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new RegistroMascotas(null,null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox cmbGenero;
    private javax.swing.JLabel imgLogo1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblCerrar;
    private javax.swing.JLabel lblPassword1;
    private javax.swing.JLabel lblPassword2;
    private javax.swing.JLabel lblPassword3;
    private javax.swing.JLabel lblPassword4;
    private javax.swing.JLabel lblTitulo1;
    private javax.swing.JLabel lblTitulo2;
    private javax.swing.JLabel lblTitulo3;
    private javax.swing.JLabel lblUsuario;
    private javax.swing.JLabel lblUsuario1;
    private javax.swing.JPanel pnlBody;
    private javax.swing.JPanel pnlFoot;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JSpinner spnEdad;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtEspecie;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtRaza;
    // End of variables declaration//GEN-END:variables
    public void Limpiar(){
        txtEspecie.setText("");
        txtNombre.setText("");
        txtRaza.setText("");
        txtDescripcion.setText("");
        spnEdad.setValue(1);
        cmbGenero.setSelectedIndex(0);
    }
}
