/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogicaNegocio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author marti
 */
public class Mascota extends AbPetApp {
    private String especie,raza,descipcion,dueño; 

    public Mascota(String nombre, String dueño, String especie, String raza, String edad, String genero, String descripcion, String estado) {
        super(nombre, edad, genero, estado);
        this.especie = especie;
        this.raza = raza;
        this.descipcion = descripcion;
        this.dueño = dueño;
    }

    public String getDueño() {
        return dueño;
    }

    public void setDueño(String dueño) {
        this.dueño = dueño;
    }


    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getDescipcion() {
        return descipcion;
    }

    public void setDescipcion(String descipcion) {
        this.descipcion = descipcion;
    }
    public String bajaMascota(){
        int id=0;
       int i=0;
       int cont=0;
       Boolean validacionUsuario=false;
       String[] dataAux= new String[10];
       File archivo = new File("src\\Archivos\\mascotas.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         String[] lineas = new String[100];
        
           while((linea = lector.readLine())!=null){
               String[]data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
               if (nombre.equals(data[1]) && dueño.equals(data[2])&&data[8].equals("activo")){
                   validacionUsuario=true;
                   dataAux = linea.split("\\|");
               }
               else{
                   lineas[i]= linea;
                   i++;
               }
               
           }
           id++;
           for(int j=0;j<i;j++)
            {
                if(cont==0){
                // System.out.println(lineas[j]);
                BufferedWriter writer = null;
                File logFile = new File("src\\Archivos\\mascotas.txt");
                writer = new BufferedWriter(new FileWriter(logFile));
                writer.write(lineas[j]+System.getProperty("line.separator"));
                writer.close();
                cont++;
                }
                else{
                    //System.out.println(lineas[j]);
                    BufferedWriter writer = null;
                    File logFile = new File("src\\Archivos\\mascotas.txt");
                    writer = new BufferedWriter(new FileWriter(logFile,true));
                    writer.write(lineas[j]+System.getProperty("line.separator"));
                    writer.close();
                    cont++;
                }
            }
           
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return "errorArchivo";
       }
        //Metodo para registrar el archivo con los datos del constructor
       if(validacionUsuario){
         if(i==0){
          try{              
            BufferedWriter writer = null;
            File logFile = new File("src\\Archivos\\mascotas.txt");
            writer = new BufferedWriter(new FileWriter(logFile));

            String usuarioTXT=(id+"|"+dataAux[1]+"|"+dataAux[2]+"|"+dataAux[3]+"|"+dataAux[4]+"|"+dataAux[5]+"|"+dataAux[6]+"|"+dataAux[7]+"|"+estado+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Mascota "+ nombre+ " dada de baja!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
          }
         else{
             try{              
             BufferedWriter writer = null;
             File logFile = new File("src\\Archivos\\mascotas.txt");
            writer = new BufferedWriter(new FileWriter(logFile,true));

             String usuarioTXT=(id+"|"+dataAux[1]+"|"+dataAux[2]+"|"+dataAux[3]+"|"+dataAux[4]+"|"+dataAux[5]+"|"+dataAux[6]+"|"+dataAux[7]+"|"+estado+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Mascota "+ nombre+ " dada de baja!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
           }
         }
       else{
           JOptionPane.showMessageDialog(null,"La mascota ingresada ya existe!!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return "errorUsuario";
       }
    }

    @Override
    public String registrarUsuario() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarUsuario() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String registrarMascota() {
       //Método para buscar el ultimo ID y sumarle uno
       int id=0;
       Boolean validacionUsuario=true;
       File archivo = new File("src\\Archivos\\mascotas.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
               if (nombre.equals(data[1]) && dueño.equals(data[2])){
                   validacionUsuario=false;
                   
               }
               
           }
           id++;
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return "errorArchivo";
       }
       
       
        //Metodo para registrar el archivo con los datos del constructor
       if(validacionUsuario){
        try{
           
        BufferedWriter writer = null;
        File logFile = new File("src\\Archivos\\mascotas.txt");
        writer = new BufferedWriter(new FileWriter(logFile,true));
       
        String usuarioTXT=(id+"|"+nombre+"|"+dueño+"|"+especie+"|"+raza+"|"+edad+"|"+genero+"|"+descipcion+"|"+estado+System.getProperty("line.separator"));
        writer.write(usuarioTXT);
        writer.close();
         JOptionPane.showMessageDialog(null,"Mascota "+ nombre+ " registrada!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
        return "exito";
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
            return "errorArchivo";
        }
       }
       else{
           JOptionPane.showMessageDialog(null,"La mascota ingresada ya existe!!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return "errorUsuario";
       }
           
    }
   
   
   
    @Override
    public String actualizarMascota() {
        
       int id=0;
       int i=0;
       int cont=0;
       Boolean validacionUsuario=false;
       File archivo = new File("src\\Archivos\\mascotas.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         String[] lineas = new String[100];
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
               if (nombre.equals(data[1]) && dueño.equals(data[2])&&data[8].equals("activo")){
                   validacionUsuario=true;                 
               }
               else{
                   lineas[i]= linea;
                   i++;
               }
               
           }
           id++;
           for(int j=0;j<i;j++)
            {
                if(cont==0){
                // System.out.println(lineas[j]);
                BufferedWriter writer = null;
                File logFile = new File("src\\Archivos\\mascotas.txt");
                writer = new BufferedWriter(new FileWriter(logFile));
                writer.write(lineas[j]+System.getProperty("line.separator"));
                writer.close();
                cont++;
                }
                else{
                    //System.out.println(lineas[j]);
                    BufferedWriter writer = null;
                    File logFile = new File("src\\Archivos\\mascotas.txt");
                    writer = new BufferedWriter(new FileWriter(logFile,true));
                    writer.write(lineas[j]+System.getProperty("line.separator"));
                    writer.close();
                    cont++;
                }
            }
           
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return "errorArchivo";
       }
        //Metodo para registrar el archivo con los datos del constructor
       if(validacionUsuario){
         if(i==0){
          try{              
            BufferedWriter writer = null;
            File logFile = new File("src\\Archivos\\mascotas.txt");
            writer = new BufferedWriter(new FileWriter(logFile));

            String usuarioTXT=(id+"|"+nombre+"|"+dueño+"|"+especie+"|"+raza+"|"+edad+"|"+genero+"|"+descipcion+"|"+estado+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Mascota "+ nombre+ " actualizada!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
          }
         else{
             try{              
             BufferedWriter writer = null;
             File logFile = new File("src\\Archivos\\mascotas.txt");
            writer = new BufferedWriter(new FileWriter(logFile,true));

            String usuarioTXT=(id+"|"+nombre+"|"+dueño+"|"+especie+"|"+raza+"|"+edad+"|"+genero+"|"+descipcion+"|"+estado+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Mascota "+ nombre+ " actualizada!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
           }
         }
       else{
           JOptionPane.showMessageDialog(null,"La mascota ingresada ya existe!!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return "errorUsuario";
       }
       
    }

    @Override
    public void ingresar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ingresarGasto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
