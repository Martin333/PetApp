/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogicaNegocio;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author marti
 */
public class Gasto {
    private String descripcion,mascota,dueño;
    private double importe;
    private Date fecha;
    private int id;

    public Gasto(int id,String descripcion, String mascota, String dueño, Date fecha,double importe) {
        this.descripcion = descripcion;
        this.mascota = mascota;
        this.dueño = dueño;
        this.importe = importe;
        this.fecha = fecha;
        this.id = id;
    }

    

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMascota() {
        return mascota;
    }

    public void setMascota(String mascota) {
        this.mascota = mascota;
    }

    public String getDueño() {
        return dueño;
    }

    public void setDueño(String dueño) {
        this.dueño = dueño;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    public int incrementarID(){
       
         //Método para buscar el ultimo ID y sumarle uno
        int id=0;
        File archivo = new File("src\\Archivos\\gastos.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
              
               
           }
           id++;
           return id;
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return id;
       }
        
    }
    public Boolean AgregarGasto(){
      
        try{  
        BufferedWriter writer = null;
        File logFile = new File("src\\Archivos\\gastos.txt");
        writer = new BufferedWriter(new FileWriter(logFile,true));
        String usuarioTXT=(id+"|"+descripcion+"|"+mascota+"|"+dueño+"|"+fecha+"|"+importe+System.getProperty("line.separator"));
        writer.write(usuarioTXT);
        writer.close();
         JOptionPane.showMessageDialog(null,"Gasto de "+descripcion+" con un costo de $"+importe+ " registrado!!! " , "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
        return true;
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
            return false;
        }
       
            
    }
       

    
 }
