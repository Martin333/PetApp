/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogicaNegocio;

/**
 *
 * @author marti
 */
public interface InPetApp {
    String registrarUsuario();
    String actualizarUsuario();
    String registrarMascota();
    String actualizarMascota();
    void ingresar();
    void ingresarGasto();
    
}

