/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogicaNegocio;

/**
 *
 * @author marti
 */
public abstract class AbPetApp implements InPetApp {

    protected String nombre,genero,estado,edad;
    

    public AbPetApp(String nombre, String edad, String genero, String estado) {
        
        this.nombre = nombre;
        this.edad = edad;
        this.genero = genero;
        this.estado = estado;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
}
