/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package LogicaNegocio;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author marti
 */
public class Usuario extends AbPetApp {
    private String usuario,password,correo,avatar;

    public Usuario(String usuario,String nombre, String password, String correo,String edad, String genero, String estado, String avatar) {
        super(nombre, edad, genero, estado);
        this.usuario = usuario;
        this.password = password;
        this.correo = correo;
        this.avatar = avatar;
    }
  

    public String getAvatar() {
       avatar= "ErrorAvatar";
       File archivo = new File("src\\Archivos\\credenciales.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         
           while((linea = lector.readLine()) !=null){
               String[] data = linea.split("\\|");
               //valida si ingresaste un usuario que ya existe
               if (usuario.equals(data[1])&&password.equals(data[3])){
                   avatar = data[8];
                   return avatar;
               }
               
           }
          
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return avatar;
       }
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    

    @Override
    public String registrarUsuario() {
      //Método para buscar el ultimo ID y sumarle uno
        int id=0;
       Boolean validacionUsuario=true;
       File archivo = new File("src\\Archivos\\credenciales.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
               if (usuario.equals(data[1])){
                   validacionUsuario=false;
                   
               }
               
           }
           id++;
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return "errorArchivo";
       }
       
       
        //Metodo para registrar el archivo con los datos del constructor
       if(validacionUsuario){
        try{
           
        BufferedWriter writer = null;
        File logFile = new File("src\\Archivos\\credenciales.txt");
        writer = new BufferedWriter(new FileWriter(logFile,true));
        String usuarioTXT=(id+"|"+usuario+"|"+nombre+"|"+password+"|"+correo+"|"+edad+"|"+genero+"|"+estado+"|"+avatar+System.getProperty("line.separator"));
        writer.write(usuarioTXT);
        writer.close();
         JOptionPane.showMessageDialog(null,"Usuario "+ nombre+ " registrado!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
        return "exito";
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
            return "errorArchivo";
        }
       }
       else{
           JOptionPane.showMessageDialog(null,"El usuario ingresado ya existe!!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return "errorUsuario";
       }
           
    }
   
    public Boolean login(){
        //Método para buscar un usuario existente
       
        Boolean validacionUsuario=false;
       File archivo = new File("src\\Archivos\\credenciales.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         
           while((linea = lector.readLine()) !=null){
               String[] data = linea.split("\\|");
               //valida si ingresaste un usuario que ya existe
               if (usuario.equals(data[1])&&password.equals(data[3])){
                   validacionUsuario=true;
               }
               
           }
           return validacionUsuario;
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return validacionUsuario;
       }
    }

    @Override
    public String actualizarUsuario() {
        
       int id=0;
       int i=0;
       int cont=0;
       Boolean validacionUsuario=false;
       File archivo = new File("src\\Archivos\\credenciales.txt");
       try{
        BufferedReader lector = new BufferedReader(new FileReader(archivo));
         String linea;
         String[] lineas = new String[100];
           while((linea = lector.readLine())!=null){
               String[] data = linea.split("\\|");
               id = Integer.parseInt(data[0]);
               //valida si ingresaste un usuario que ya existe
               if (usuario.equals(data[1])&&data[7].equals("activo") ){
                   validacionUsuario=true;                 
               }
               else{
                   lineas[i]= linea;
                   i++;
               }
               
           }
           id++;
           for(int j=0;j<i;j++)
            {
                if(cont==0){
                 System.out.println(lineas[j]);
                BufferedWriter writer = null;
                File logFile = new File("src\\Archivos\\credenciales.txt");
                writer = new BufferedWriter(new FileWriter(logFile));
                writer.write(lineas[j]+System.getProperty("line.separator"));
                writer.close();
                cont++;
                }
                else{
                    System.out.println(lineas[j]);
                    BufferedWriter writer = null;
                    File logFile = new File("src\\Archivos\\credenciales.txt");
                    writer = new BufferedWriter(new FileWriter(logFile,true));
                    writer.write(lineas[j]+System.getProperty("line.separator"));
                    writer.close();
                    cont++;
                }
            }
           
    
       }
       catch(Exception e){
          JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
          return "errorArchivo";
       }
        //Metodo para registrar el archivo con los datos del constructor
       if(validacionUsuario){
         if(i==0){
          try{              
            BufferedWriter writer = null;
            File logFile = new File("src\\Archivos\\credenciales.txt");
            writer = new BufferedWriter(new FileWriter(logFile));

             String usuarioTXT=(id+"|"+usuario+"|"+nombre+"|"+password+"|"+correo+"|"+edad+"|"+genero+"|"+estado+"|"+avatar+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Usuario "+ nombre+ " actualizado!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
          }
         else{
             try{              
             BufferedWriter writer = null;
             File logFile = new File("src\\Archivos\\credenciales.txt");
            writer = new BufferedWriter(new FileWriter(logFile,true));

             String usuarioTXT=(id+"|"+usuario+"|"+nombre+"|"+password+"|"+correo+"|"+edad+"|"+genero+"|"+estado+"|"+avatar+System.getProperty("line.separator"));
            writer.write(usuarioTXT);
            writer.close();
             JOptionPane.showMessageDialog(null,"Usuario "+ nombre+ " actualizado!!", "ÉXITO", JOptionPane.INFORMATION_MESSAGE,null);
            return "exito";
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null,"Ocurrió un error inesperado!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
                return "errorArchivo";
            }
           }
         }
       else{
           JOptionPane.showMessageDialog(null,"El usuario ingresado ya existe!!", "ERROR", JOptionPane.WARNING_MESSAGE,null);
           return "errorUsuario";
       }
       
    }

    @Override
    public String registrarMascota() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String actualizarMascota() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ingresar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void ingresarGasto() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
